from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from .forms import *
from django.http import HttpResponse

# Create your views here.
def signup_view(request):
    form = CreateUserForm()

    if request.user.is_authenticated:
        return HttpResponse("You're already logged in. Please log out first to register a new account.")

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')

    return render(request, 'accounts/signup.html', {'form': form})

def login_view(request):
    form = LoginUserForm()

    if request.user.is_authenticated:
        return HttpResponse("You're already logged in. Please log out first to log in from other account.")

    if request.method == "POST":
        form = LoginUserForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST.get('next'))
            else:
                return redirect('/')

    return render(request, 'accounts/login.html', {'form': form})

def logout_view(request):
    logout(request)
    return redirect('/')


