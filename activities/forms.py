from django import forms
from django.forms import ModelForm
from .models import *

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Activity name'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            'desc': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'A brief description of the activity'}),
            'participants': forms.TextInput(attrs={'class': 'form-control'}),
        } 

class ParticipantForm(ModelForm):
    class Meta:
        model = Participant
        fields = '__all__'

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Participant name'}),
            
        } 