from django.db import models

# Create your models here.
class Participant(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Activity(models.Model):

    class ActivityCategory(models.TextChoices):
        general = "General"
        informative = "Informative"
        fun = "Fun"
        important = "Important"
        others = "Others"

    name = models.CharField(max_length=100, unique=True)
    category = models.CharField(
        max_length=20,
        choices=ActivityCategory.choices,
        default=ActivityCategory.general,
        )
    desc = models.CharField(max_length=200, blank=True, null=True)
    participants = models.ManyToManyField(Participant, blank=True)

    def __str__(self):
        return self.name