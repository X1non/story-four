from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .models import *

# Create your tests here.
class ActivityTest(TestCase):

    def test_activity_url_is_exist(self):
        response = Client().get('/activities/')
        self.assertEquals(response.status_code, 200)

    def test_activity_using_list_activities_function(self):
        found = resolve('/activities/')
        self.assertEquals(found.func, list_activities)

    def test_activity_page_using_template(self):
        response = Client().get('/activities/')
        self.assertTemplateUsed(response, "activities/list_activities.html")

    def test_activity_web_title(self):
        response = Client().get('/activities/')
        html_response = response.content.decode('utf8')
        self.assertIn("Activities", html_response)
        self.assertIn("Create Activity", html_response)

    def test_activity_model_exist(self):
        Activity.objects.create(name="Name", category="General", desc="OwO whats this?")
        self.assertEquals(Activity.objects.all().count(), 1)

    def test_participant_model_exist(self):
        Participant.objects.create(name="Bambang")
        self.assertEquals(Participant.objects.all().count(), 1)

    def test_activity_added_on_page(self):
        Activity.objects.create(name="code n fun", category="Fun", desc="codecodecode")
        response = Client().get('/activities/')
        html_response = response.content.decode('utf8')
        self.assertIn("code n fun", html_response)
        self.assertIn("codecodecode", html_response)
        self.assertIn("Fun", html_response)
        self.assertIn("Edit", html_response)
        self.assertIn("Add Activity", html_response)
        self.assertIn("Add Participant", html_response)
        self.assertIn("Delete", html_response)
        self.assertIn("Details", html_response)


class ActivityAddTest(TestCase):

    def setUp(self):
        self.activity1 = Activity.objects.create(name="Name", category="General", desc="OwO whats this?")

    def test_add_activity_url_is_exist(self):
        response = Client().get('/activities/create-activity')
        self.assertEquals(response.status_code, 200)

    def test_add_activity_using_create_activity_function(self):
        found = resolve('/activities/create-activity')
        self.assertEquals(found.func, create_activity)

    def test_activity_page_using_template(self):
        response = Client().get('/activities/create-activity')
        self.assertTemplateUsed(response, "activities/activities_form.html")

    def test_add_activity_title(self):
        response = Client().get('/activities/create-activity')
        html_response = response.content.decode('utf8')
        self.assertIn("Create Activity", html_response)
        self.assertIn("Create", html_response)
        self.assertIn("Go Back", html_response)

    def test_edit_activity_title(self):
        response = Client().get('/activities/edit-activity/Name')
        html_response = response.content.decode('utf8')
        self.assertIn("Edit Activity", html_response)
        self.assertIn("Edit", html_response)
        self.assertIn("Go Back", html_response)

    def test_form_add_activity_valid_data(self):
        form = ActivityForm(data={
            'name': 'Farming Simulator',
            'category': 'General',
            'desc': 'Farming sim, farming life'
        })

        self.assertTrue(form.is_valid())

    def test_form_add_activity_empty(self):
        form = ActivityForm(data={

        })

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)

class AddParticipantTest(TestCase):

    def setUp(self):
        self.activity1 = Activity.objects.create(name="Name", category="General", desc="OwO whats this?")
    
    def test_add_participant_url_is_exist(self):
        response = Client().get('/activities/1/add-participant')
        self.assertEquals(response.status_code, 200)

    def test_add_participant_using_add_participant_function(self):
        found = resolve('/activities/1/add-participant')
        self.assertEquals(found.func, add_participants)

    def test_add_participant_page_using_template(self):
        response = Client().get('/activities/1/add-participant')
        self.assertTemplateUsed(response, "activities/add_participant.html")

    def test_add_participant_viewpage(self):
        response = Client().get('/activities/1/add-participant')
        html_response = response.content.decode('utf8')
        self.assertIn("Add Participant", html_response)
        self.assertIn("Add", html_response)
        self.assertIn("Go Back", html_response)

    def test_form_add_participant_valid_data(self):
        form = ParticipantForm(data={
            'name': 'Bambang',
        })

        self.assertTrue(form.is_valid())

    def test_form_add_participant_empty(self):
        form = ParticipantForm(data={

        })

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)

class ActivityDetailsTest(TestCase):
    def setUp(self):
        self.activity1 = Activity.objects.create(name="Name", category="General", desc="OwO whats this?")
    
    def test_details_url_is_exist(self):
        response = Client().get('/activities/details')
        self.assertEquals(response.status_code, 200)

    def test_details_function(self):
        found = resolve('/activities/details')
        self.assertEquals(found.func, details)

    def test_details_page_using_template(self):
        response = Client().get('/activities/details')
        self.assertTemplateUsed(response, "activities/details.html")

    def test_details_viewpage(self):
        response = Client().get('/activities/details')
        html_response = response.content.decode('utf8')
        self.assertIn("Details", html_response)
        self.assertIn("Activity", html_response)
        self.assertIn("Description", html_response)
        self.assertIn("Category", html_response)
        self.assertIn("Participants", html_response)
