from django.urls import path

from . import views

app_name = 'activities'

urlpatterns = [
    path('', views.list_activities, name='list_activities'),
    path('create-activity', views.create_activity, name='create_activity'),
    path('edit-activity/<str:name>', views.edit_activity, name='edit_activity'),
    path('delete-activity/<str:name>', views.delete_activity, name='delete_activity'),
    path('<int:activity_id>/add-participant', views.add_participants, name='add_participant'),
    path('details', views.details, name='details')
]