from django.test import TestCase, Client
from django.urls import resolve
# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
# from webdriver_manager.chrome import ChromeDriverManager
from .views import *
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.chrome.options import Options

# Create your tests here.

class BookSearch(TestCase):

    def test_index_url_is_exist(self):
        response = Client().get('/book-search/')
        self.assertEquals(response.status_code, 200)

    def test_index_using_index_function(self):
        found = resolve('/book-search/')
        self.assertEquals(found.func, index)

    def test_index_page_using_template(self):
        response = Client().get('/book-search/')
        self.assertTemplateUsed(response, "booksearch/index.html")

    def test_index_components(self):
        response = Client().get('/book-search/')
        html_response = response.content.decode('utf8')
        self.assertIn("Search", html_response)
        self.assertIn("Result", html_response)
        self.assertIn("Insert the book", html_response)

# class TestBookSearchBar(StaticLiveServerTestCase):
#     def test_book_url_is_exist(self):
#         response = Client().post('/book-data/', {'q': 'Spongebob Naturepants'})
#         self.assertEquals(response.status_code, 200)


    # def setUp(self):
    #     self.credentials = {
    #         'username': 'testuser',
    #         'password': 'secret'}
    #     User.objects.create_user(**self.credentials)

    #     chrome_options = Options()  
    #     chrome_options.add_argument("--headless") 
    #     chrome_options.add_argument('--no-sandbox')
    #     chrome_options.add_argument('--disable-dev-shm-usage')
    #     self.selenium = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)
    #     super(TestBookSearchBar, self).setUp()
        

    # def test_search_book(self):
    #     self.client.login(username='testuser', password='secret')
    #     self.browser = self.selenium
    #     self.browser.get(self.live_server_url + '/book-search/')

        
    #     wait = WebDriverWait(self.browser, 20)
    #     wait.until(
    #         EC.presence_of_element_located((By.ID, 'search-keywords'))
    #     )

    #     search = self.browser.find_element_by_id('search-keywords')
    #     search.click()
    #     search.send_keys("Spongebob Naturepants")

    #     wait.until(EC.presence_of_element_located((By.XPATH, '//th[text()="Spongebob Naturepants"]')))

    #     assert "Spongebob Naturepants" in browser.page_source
            
