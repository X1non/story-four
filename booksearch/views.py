from django.shortcuts import render
from django.http import JsonResponse
import json
import requests
# Create your views here.
def index(request):
    return render(request, "booksearch/index.html")

def book_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']   # getting the API's URL corresponding to the param
    resp = requests.get(url)    # from the request we get the response, response.content is the JSON (content) we're looking for
    bookData = json.loads(resp.content)  # Here we parse the JSON string (resp.content) into python dictionaries
    return JsonResponse(bookData, safe=False)   # Return the data into JSON as a response