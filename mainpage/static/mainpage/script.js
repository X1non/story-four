$(document).ready(function(){
    $('.up-btn').click(function(event){
        $(this).parents('.accordion').insertBefore($(this).parents('.accordion').prev());
        $(this).focus();
        event.stopPropagation();
        
    });

    $('.down-btn').click(function(event){
        $(this).parents('.accordion').insertAfter($(this).parents('.accordion').next());
        $(this).focus();
        event.stopPropagation();
    });

    $('.accordion').click(function(){
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('.accordion-content').hide(250);
        }

        else {
            $(this).addClass('active');
            $(this).find('.accordion-content').show(250);
        }
    });

});
