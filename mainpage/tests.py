from django.test import TestCase, Client
from django.urls import resolve
from .views import *
# Create your tests here.

class MainpageTest(TestCase):

    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_about_url_is_exist(self):
        response = Client().get('/about')
        self.assertEquals(response.status_code, 200)

    def test_gallery_url_is_exist(self):
        response = Client().get('/gallery')
        self.assertEquals(response.status_code, 200)

    def test_coming_soon_url_is_exist(self):
        response = Client().get('/coming-soon')
        self.assertEquals(response.status_code, 200)


    def test_homepage_using_home_function(self):
        found = resolve('/')
        self.assertEquals(found.func, home)

    def test_about_using_about_function(self):
        found = resolve('/about')
        self.assertEquals(found.func, about)

    def test_gallery_using_gallery_function(self):
        found = resolve('/gallery')
        self.assertEquals(found.func, gallery)

    def test_coming_soon_using_comingsoon_function(self):
        found = resolve('/coming-soon')
        self.assertEquals(found.func, comingsoon)


    def test_homepage_page_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "mainpage/index.html")

    def test_about_page_using_template(self):
        response = Client().get('/about')
        self.assertTemplateUsed(response, "mainpage/about.html")

    def test_gallery_page_using_template(self):
        response = Client().get('/gallery')
        self.assertTemplateUsed(response, "mainpage/gallery.html")

    def test_comingsoon_page_using_template(self):
        response = Client().get('/coming-soon')
        self.assertTemplateUsed(response, "mainpage/other.html")


    def test_homepage_components(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Timothy Efraim", html_response)
        self.assertIn("Welcome aboard!", html_response)
        self.assertIn("gallery", html_response)
        self.assertIn("about me", html_response)

    def test_accordion_components(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("tldr", html_response)
        self.assertIn("Bio", html_response)
        self.assertIn("Education", html_response)
        self.assertIn("down-arrow-symbol-in-black-square-button.svg", html_response)
        self.assertIn("Majoring", html_response)
        self.assertIn("★", html_response)

    def test_about_components(self):
        response = Client().get('/about')
        html_response = response.content.decode('utf8')
        self.assertIn("About Me", html_response)
        self.assertIn("Education", html_response)
        self.assertIn("Experience", html_response)
        self.assertIn("Skills", html_response)

    def test_gallery_components(self):
        response = Client().get('/gallery')
        html_response = response.content.decode('utf8')
        self.assertIn("Gallery", html_response)
        self.assertIn("Pictures", html_response)
        self.assertIn("1.jpg", html_response)

    def test_comingsoon_components(self):
        response = Client().get('/coming-soon')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello.", html_response)
