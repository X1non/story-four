from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'mainpage/index.html')

def gallery(request):
    return render(request, 'mainpage/gallery.html')

def about(request):
    return render(request, 'mainpage/about.html')

def comingsoon(request):
    return render(request, 'mainpage/other.html')