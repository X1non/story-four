from django import forms
from django.forms import ModelForm
from .models import Matkul

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul      # Decide the model
        fields = '__all__'  # create form with all the attr. contained in class Matkul (Models.py)

        widgets = {
            'nama': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Course name'}),
            'singkatan': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Course code/abbreviation'}),
            'dosen': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Lecturer of the course'}),
            'sks': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'SKS/course credit'}),
            'desc': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'A brief description of the course'}),
            'semester': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Course semester'}),
            'kelas': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your classroom'}),
        } 