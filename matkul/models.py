from django.db import models

# Create your models here.

class Matkul(models.Model):
    nama = models.CharField(max_length=100, unique=True)
    singkatan = models.SlugField(unique=True)
    dosen = models.CharField(max_length=100, null=True)
    sks = models.IntegerField()
    desc = models.TextField(null=True, blank=True)
    semester = models.CharField(max_length=20, null=True)
    kelas = models.CharField(max_length=1, null=True)
    
    def __str__(self):
        return self.nama