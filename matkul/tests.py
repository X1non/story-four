from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .models import *

# Create your tests here.
class MatkulTest(TestCase):

    def test_course_url_is_exist(self):
        response = Client().get('/courses/')
        self.assertEquals(response.status_code, 200)

    def test_course_using_list_matkul_function(self):
        found = resolve('/courses/')
        self.assertEquals(found.func, list_matkul)

    def test_course_page_using_template(self):
        response = Client().get('/courses/')
        self.assertTemplateUsed(response, "matkul/matkul.html")

    def test_course_web_components(self):
        response = Client().get('/courses/')
        html_response = response.content.decode('utf8')
        self.assertIn("Courses", html_response)
        self.assertIn("Add Course", html_response)

    def test_course_model_exist(self):
        Matkul.objects.create(
            nama="test",
            singkatan="test",
            dosen="test",
            sks=1,
            desc="test",
            semester="test",
            kelas="T"
            )
        self.assertEquals(Matkul.objects.all().count(), 1)

    def test_course_added_on_page(self):
        Matkul.objects.create(
            nama="test",
            singkatan="test",
            dosen="test",
            sks=1,
            desc="test",
            semester="test",
            kelas="T"
            )
        response = Client().get('/courses/')
        html_response = response.content.decode('utf8')
        self.assertIn("test", html_response)
        self.assertIn("Details", html_response)
        self.assertIn("Update", html_response)
        self.assertIn("Delete", html_response)
        


class AddMatkulTest(TestCase):

    def setUp(self):
        Matkul.objects.create(
            nama="test",
            singkatan="test",
            dosen="test",
            sks=1,
            desc="test",
            semester="test",
            kelas="T"
            )

    def test_create_course_url_is_exist(self):
        response = Client().get('/courses/create-course')
        self.assertEquals(response.status_code, 200)

    def test_create_course_using_create_function(self):
        found = resolve('/courses/create-course')
        self.assertEquals(found.func, create_matkul)

    def test_course_page_using_template(self):
        response = Client().get('/courses/create-course')
        self.assertTemplateUsed(response, "matkul/matkul_form.html")

    def test_create_course_components(self):
        response = Client().get('/courses/create-course')
        html_response = response.content.decode('utf8')
        self.assertIn("Add Course", html_response)
        self.assertIn("Create", html_response)
        self.assertIn("Go Back", html_response)
        self.assertIn("Nama:", html_response)

    def test_update_course_components(self):
        response = Client().get('/courses/update-course/test')
        html_response = response.content.decode('utf8')
        self.assertIn("Update Course", html_response)
        self.assertIn("Update", html_response)
        self.assertIn("Go Back", html_response)
        self.assertIn("Nama:", html_response)


    def test_form_add_course_valid_data(self):
        form = MatkulForm(data={
            "nama":"test2",
            "singkatan":"test2",
            "dosen":"test2",
            "sks":1,
            "desc":"test2",
            "semester":"test",
            "kelas":"T"
        })

        self.assertTrue(form.is_valid())

    def test_form_add_course_empty(self):
        form = MatkulForm(data={

        })

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 6)

class DetailMatkulTest(TestCase):
    def setUp(self):
        Matkul.objects.create(
            nama="test3",
            singkatan="test3",
            dosen="test3",
            sks=1,
            desc="test3",
            semester="test3",
            kelas="T"
            )

    def test_details_course_url_is_exist(self):
        response = Client().get('/courses/course-details/test3')
        self.assertEquals(response.status_code, 200)

    def test_details_course_using_create_function(self):
        found = resolve('/courses/course-details/test3')
        self.assertEquals(found.func, matkul_desc)

    def test_details_page_using_template(self):
        response = Client().get('/courses/course-details/test3')
        self.assertTemplateUsed(response, "matkul/matkul_content.html")

    def test_details_course_components(self):
        response = Client().get('/courses/update-course/test3')
        html_response = response.content.decode('utf8')
        self.assertIn("test3", html_response)
        self.assertIn("Go Back", html_response)
        

class DetailMatkulWarningTest(TestCase):
    def setUp(self):
        Matkul.objects.create(
            nama="test4",
            singkatan="test4",
            dosen="test4",
            sks=1,
            desc="",
            semester="test4",
            kelas="T"
            )

    def test_details_course_url_is_exist(self):
        response = Client().get('/courses/course-details/test4')
        self.assertEquals(response.status_code, 200)

    def test_details_course_using_create_function(self):
        found = resolve('/courses/course-details/test4')
        self.assertEquals(found.func, matkul_desc)

    def test_details_page_using_template(self):
        response = Client().get('/courses/course-details/test4')
        self.assertTemplateUsed(response, "matkul/matkul_warning.html")

    def test_details_course_components(self):
        response = Client().get('/courses/update-course/test4')
        html_response = response.content.decode('utf8')
        self.assertIn("Go Back", html_response)