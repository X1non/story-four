from django.shortcuts import render, redirect
from .models import Matkul
from .forms import MatkulForm
# Create your views here.

def list_matkul(request):
    list_matkul = Matkul.objects.all()
    return render(request,'matkul/matkul.html', {'list_matkul':list_matkul})


def matkul_desc(request, slug):
    matkul = Matkul.objects.get(singkatan=slug)

    if not matkul.desc:
        context = {'matkul':matkul}
        return render(request, 'matkul/matkul_warning.html', context)
    
    context = {'matkul':matkul}
    return render(request, 'matkul/matkul_content.html', context)


def create_matkul(request):
    form = MatkulForm()

    if request.method == "POST":
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/courses")

    context = {'form':form}
    return render(request, 'matkul/matkul_form.html', context)


def update_matkul(request, slug):
    matkul = Matkul.objects.get(singkatan=slug)
    form = MatkulForm(instance=matkul)

    if request.method == "POST":
        form = MatkulForm(request.POST, instance=matkul)
        if form.is_valid():
            form.save()
            return redirect("/courses")

    context = {'form':form}
    return render(request, 'matkul/matkul_form.html', context)


def delete_matkul(request, slug):
    matkul = Matkul.objects.get(singkatan=slug)

    if request.method == "POST":
        matkul.delete()
        return redirect("/courses")

    context = {'matkul':matkul}
    return render(request, 'matkul/matkul.html', context)

