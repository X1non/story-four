from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Story1Test(TestCase):

    def test_story1_url_is_exist(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)

    def test_story1_using_story1_function(self):
        found = resolve('/story1/')
        self.assertEquals(found.func, story1)

    def test_story1_page_using_template(self):
        response = Client().get('/story1/')
        self.assertTemplateUsed(response, "story1/index.html")

    def test_story1_components(self):
        response = Client().get('/story1/')
        html_response = response.content.decode('utf8')
        self.assertIn("Timothy Efraim", html_response)
        self.assertIn("Hello World", html_response)
        self.assertIn("About Me", html_response)
        self.assertIn("Contacts", html_response)